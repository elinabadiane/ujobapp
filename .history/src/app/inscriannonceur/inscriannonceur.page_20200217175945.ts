import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InscriptionService } from '../services/inscription.service';

@Component({
  selector: 'app-inscriannonceur',
  templateUrl: './inscriannonceur.page.html',
  styleUrls: ['./inscriannonceur.page.scss'],
})
export class InscriannonceurPage implements OnInit {
  imageUrl = '/assets/images/14479616_1114726928611996_756676038217360505_n.jpg';
  userData = {imageFile : File = null};

  constructor(private appService: InscriptionService,
              private router: Router) { }

  ngOnInit() {
  }
  handleFileInput(file: FileList) {
    this.userData.imageFile = file.item(0);
    const reader = new FileReader();
    reader.onload = (event: any) => {
    this.imageUrl = event.target.result;
    };
    reader.readAsDataURL(this.userData.imageFile);
    }
    user() {
    this.appService.User(this.userData)
    .subscribe(
         res => {
    console.log(res);
    this.router.navigate(['/']);

      },
    );

}

}
