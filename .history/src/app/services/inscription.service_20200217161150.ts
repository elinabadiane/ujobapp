import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InscriptionService {

  constructor(private httpClient: HttpClient) { }
  formUser(User) {
    const endpoint = 'http://localhost:8000/usercompte';
    const formData: FormData = new FormData();
    formData.append('nomentrep', User.nomentrep);
    formData.append('logo', User.logo);
    formData.append('adressentrep', User.adressentrep);
    formData.append('descriptionentrep', User.descriptionentrep);
    formData.append('siteinternet', User.siteinternet);
    formData.append('secteuractivite', User.secteuractivite);
    formData.append('username', User.username);
    formData.append('password', User.password);
    formData.append('confirmepassword', User.confirmepassword);
    formData.append('telephone', User.telephone);
    formData.append('telephone1', User.telephone1);
    return this.httpClient.post(endpoint, formData);
    }

}
