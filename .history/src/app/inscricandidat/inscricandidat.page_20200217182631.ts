import { Component, OnInit } from '@angular/core';
import { InscriptionService } from '../services/inscription.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inscricandidat',
  templateUrl: './inscricandidat.page.html',
  styleUrls: ['./inscricandidat.page.scss'],
})
export class InscricandidatPage implements OnInit {
  imageUrl = '/assets/images/14479616_1114726928611996_756676038217360505_n.jpg';
  userData = {imageFile : File = null};

  constructor(private appService: InscriptionService,
              private router: Router) { }

  ngOnInit() {
  }

}
