import { Component, OnInit } from '@angular/core';
import { InscriptionService } from '../services/inscription.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inscricandidat',
  templateUrl: './inscricandidat.page.html',
  styleUrls: ['./inscricandidat.page.scss'],
})
export class InscricandidatPage implements OnInit {
  imageUrl = '/assets/images/14479616_1114726928611996_756676038217360505_n.jpg';
  ajoutData = {imageFile : File = null};

  constructor(private appService: InscriptionService,
              private router: Router) { }

  ngOnInit() {
  }
  handleFileInput(file: FileList) {
    this.ajoutData.imageFile = file.item(0);
    const reader = new FileReader();
    reader.onload = (event: any) => {
    this.imageUrl = event.target.result;
    };
    reader.readAsDataURL(this.ajoutData.imageFile);
    }
    ajout() {
    this.appService.Ajout(this.ajoutData)
    .subscribe(
         res => {
    this.router.navigate(['/']);

      },
      err => {
        console.log(err)
  }
    );

}
}
