import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'connexion',
    loadChildren: () => import('./connexion/connexion.module').then( m => m.ConnexionPageModule)
  },
  {
    path: 'choixprofil',
    loadChildren: () => import('./choixprofil/choixprofil.module').then( m => m.ChoixprofilPageModule)
  },
  {
    path: 'inscriannonceur',
    loadChildren: () => import('./inscriannonceur/inscriannonceur.module').then( m => m.InscriannonceurPageModule)
  },
  {
    path: 'inscricandidat',
    loadChildren: () => import('./inscricandidat/inscricandidat.module').then( m => m.InscricandidatPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
