import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ConnexionService } from '../services/connexion.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {
  loginUserData = {};
  helper = new JwtHelperService();
  rol = {};
  constructor(private auth: ConnexionService,
              private router: Router) { }

  ngOnInit() {
  }

  loginUser() {
    this.auth.loginUser(this.loginUserData)
    .subscribe(
      res => {
        console.log(this.loginUserData);
        console.log(res);
        if ( res.token) {
          localStorage.setItem('token', res.token);
          const decodedToken = this.helper.decodeToken(res.token);
          console.log(decodedToken.username);
          localStorage.setItem('username', decodedToken.username);
          localStorage.setItem('roles', decodedToken.roles);
          localStorage.setItem('expiration', decodedToken.exp);
          console.log(localStorage);
   }
  }
   );
    this.router.navigateByUrl('/menu/');

  }


}
