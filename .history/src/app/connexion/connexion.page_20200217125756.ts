import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {
  loginUserData = {};
  helper = new JwtHelperService();
  rol = {};
  constructor(private auth: AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  loginUser() {
    this.auth.loginUser(this.loginUserData)
    .subscribe(
      res => {
        console.log(this.loginUserData);
        console.log(res);
        if ( res.token) {
          localStorage.setItem('token', res.token);
          const decodedToken = this.helper.decodeToken(res.token);
          console.log(decodedToken.username);
          localStorage.setItem('username', decodedToken.username);
          localStorage.setItem('roles', decodedToken.roles);
          localStorage.setItem('expiration', decodedToken.exp);
          console.log(localStorage);
   }
   this.router.navigateByUrl('/menu/transaction');
   );
  }


}
