import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConnexionService } from '../services/connexion.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  menus = [
    {title: 'Home', url: 'home', icon : 'log-out'},
    {title: 'Connexion', url: '/menu/connexion', icon : 'log-in'},
    {title: 'Choixprofil', url: '/menu/choixprofil', icon : 'share'},
    {title: 'Inscriannonceur', url: '/menu/inscriannonceur', icon : 'sync'},
    {title: 'Inscricandidat', url: '/menu/inscricandidat', icon : 'school'},

];
  constructor(private router: Router, private authService: ConnexionService) { }
  onMenuItem(m) {
    this.router.navigateByUrl(m.url);
  }
  ngOnInit() {
  }

}
