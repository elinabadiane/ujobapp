import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChoixprofilPageRoutingModule } from './choixprofil-routing.module';

import { ChoixprofilPage } from './choixprofil.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChoixprofilPageRoutingModule
  ],
  declarations: [ChoixprofilPage]
})
export class ChoixprofilPageModule {}
