import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChoixprofilPage } from './choixprofil.page';

const routes: Routes = [
  {
    path: '',
    component: ChoixprofilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChoixprofilPageRoutingModule {}
