import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InscriannonceurPageRoutingModule } from './inscriannonceur-routing.module';

import { InscriannonceurPage } from './inscriannonceur.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InscriannonceurPageRoutingModule
  ],
  declarations: [InscriannonceurPage]
})
export class InscriannonceurPageModule {}
