import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InscriannonceurPage } from './inscriannonceur.page';

describe('InscriannonceurPage', () => {
  let component: InscriannonceurPage;
  let fixture: ComponentFixture<InscriannonceurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscriannonceurPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InscriannonceurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
