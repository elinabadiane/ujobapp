import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InscriannonceurPage } from './inscriannonceur.page';

const routes: Routes = [
  {
    path: '',
    component: InscriannonceurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InscriannonceurPageRoutingModule {}
