import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InscricandidatPage } from './inscricandidat.page';

describe('InscricandidatPage', () => {
  let component: InscricandidatPage;
  let fixture: ComponentFixture<InscricandidatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscricandidatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InscricandidatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
